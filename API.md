# API Documentation

## buildbycords

### Input parametrs

cords - []int // Координаты (Результаты ответов на вопросы)
sess - string // Идентификатор текущей сессии

### OutPut Prarametrs

idBuild - []build // Список готовых сборок


## getallbuilds

### Input parametrs

sess - string // Идентификатор текущей сессии

### OutPut Prarametrs

idBuild - []build // Список готовых сборок

## addbuild

### Input parametrs

session - string // Идентификатор текущей сессии
build - obj // Список идентификаторов компонентов
cords - []int // Координаты. Ответы на вопросы
description - string // Описание сборки
name - string // Название сборки

### OutPut Prarametrs

status - bool // Результат добавления сборки в БД

## changepass

### Input parametrs

session - string // Идентификатор текущей сессии
password - string // Текущий пароль пользователя

### OutPut Prarametrs

status - bool // Результат изменеия пароля.

## signup

### Input parametrs

login - string // Логин пользователя для входа в систему 
pass - string // Пароль пользователя
name - string // Имя для отображения пользователя в системе

### OutPut Prarametrs

session - string // Идентификатор текущей сессии

## signin

### Input parametrs

login - string // Логин пользователя для входа в систему 
pass - string // Пароль пользователя

### OutPut Prarametrs

session - string // Идентификатор текущей сессии