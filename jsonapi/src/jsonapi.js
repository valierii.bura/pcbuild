const crypto = require('crypto');
const mongo = require('mongodb');
const {
    DATABASE_ERROR,
    SESSION_NOT_FOUND,
    EMPTY_FIELD,
    BAD_PASS,
    IDENTIFY_PASS,
    UNKNOWN_TYPE,
    HAVENT_PERMISSION,
    COMP_NOT_FOUND,
    BAD_BSON,
    BUILD_NOT_FOUND,
} = require('./status.js');
/* Constant for signIn/signUp */
const keylen = 64,
    iterations = 10000,
    digest = 'sha256';

const comptypes = [
    'cpu',
    'hdd',
    'ozu',
    'ssd',
    'blockculler',
    'block',
    'cpuculler',
    'liquidcooling',
    'motherboard',
    'powersupply',
    'videocard'
];

const rusAccTypes = {
    'user': 'Пользователь',
    'moderator': 'Модератор',
    'admin': 'Администратор',
    'root': 'Root'
};

const accTypes = ['user', 'moderator', 'admin', 'root'];
const buildStatuses = ['', 'approve', 'disapprove'];


const checkBuild = (build) => {
    if (build.cpu === undefined || build.cpu.length == 0) return 'cpu';

    if (build.motherboard === undefined || build.motherboard.length == 0) return 'motherboard';

    if (build.block === undefined || build.block.length == 0) return 'block';

    if (build.cpuculler === undefined || build.cpuculler.length == 0) return 'cpuculler';

    if (build.ozu === undefined || build.ozu.length == 0) return 'ozu';

    //   if (build.drive === undefined || build.drive.length == 0) return 'drive';

    if (build.drive.type === undefined || build.drive.type.length == 0) return 'drive type';

    if (build.powersupply === undefined || build.powersupply.length == 0) return 'powersupply';
}

module.exports.moderatebuild = async(body) => {
    const { session, build, status } = body;

    if (build === undefined || build.length == 0) {
        EMPTY_FIELD.field = 'build';
        throw EMPTY_FIELD;
    }

    if (buildStatuses.indexOf(status) == -1) {
        UNKNOWN_TYPE.msg += ' статуса';
        throw UNKNOWN_TYPE;
    }

    const ses = await checksession({ session });

    if (accTypes.indexOf(ses.type) < 1) throw HAVENT_PERMISSION;

    let _id;

    try {
        _id = new mongo.ObjectID(build);
    } catch (e) {
        throw BAD_BSON;
    }

    const cb = dbo.collection('build');
    let pcbuild;

    try {
        pcbuild = await cb.findOne({ _id });
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (pcbuild === null) throw BUILD_NOT_FOUND;

    const update = { $set: { status } };

    try {
        await cb.updateOne({ _id }, update);
    } catch (e) {
        throw DATABASE_ERROR;
    }

};

// Function for getBuildByID
module.exports.getbuild = async(body) => {
    const { session, buildid } = body;

    if (buildid == undefined || buildid.length == 0) {
        EMPTY_FIELD.field = 'buildid';
        throw EMPTY_FIELD;
    }

    const ses = await checksession({ session });

    const cb = dbo.collection('build');
    let _id;

    try {
        _id = new mongo.ObjectID(buildid);
    } catch (e) {
        throw BAD_BSON;
    }

    let build;
    try {
        build = await cb.findOne({ _id });
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (build === null) throw COMP_NOT_FOUND;

    await new Promise(async(rslv, rej) => {
        for (const comp of comptypes) {
            let _id;

            try {
                _id = new mongo.ObjectID(build[comp]);
            } catch (e) {
                throw BAD_BSON;
            }

            let component;
            const cc = dbo.collection(comp);
            try {
                component = await cc.findOne({ _id });
            } catch (e) {
                console.log(e);
                throw DATABASE_ERROR;
            }

            build[comp] = component;
        }
        rslv();
    });

    return build;
}

module.exports.components = async() => {
    return comptypes;
}

// Method for get build by cords
module.exports.buildbycords = async(body) => {
    const { cords } = body;

    if (!cords && cords.length == 0) {
        EMPTY_FIELD.field = 'cords';
        throw EMPTY_FIELD;
    }

    let m = {};
    m['status'] = 'approve';

    let vectors = [];

    let builds;
    const cb = dbo.collection('build');
    try {
        builds = await cb.find(m).toArray();
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (builds === null || builds.length === 0) throw COMP_NOT_FOUND;
    let idBuild = {};

    await new Promise(async(rslv, rej) => {
        for (let index = 0; index < builds.length; index++) {
            const build = builds[index];

            const val1 = build.cords[0] - cords[0];
            const val2 = build.cords[1] - cords[1];

            const vector = Math.sqrt(val1 * val1 + val2 * val2);
            vectors.push(vector);

            for (const comp of comptypes) {
                let _id;

                try {
                    _id = new mongo.ObjectID(build[comp]);
                } catch (e) {
                    throw BAD_BSON;
                }

                let component;
                const cc = dbo.collection(comp);
                try {
                    component = await cc.findOne({ _id });
                } catch (e) {
                    console.log(e);
                    throw DATABASE_ERROR;
                }

                builds[index][comp] = component;
            }

            let uid;

            try {
                uid = new mongo.ObjectID(build.owner);
            } catch (e) {
                throw BAD_BSON;
            }

            let user;
            let cu = dbo.collection('account');
            try {
                user = await cu.findOne({ _id: uid });
            } catch (e) {
                console.log(e);
                throw DATABASE_ERROR;
            }

            if (user && user.name) {
                builds[index]['username'] = user.name;
            }

            if (!idBuild[vector]) idBuild[vector] = [];
            idBuild[vector].push(builds[index]);
        }
        rslv();
    });

    const best = Math.min.apply(null, vectors);
    idBuild[best][0]['best'] = best;
    return idBuild[best];
}

module.exports.approvebuild = async(body) => {
    const { session, buildid, status, disapprovereason } = body;

    if (!session || session.length == 0) {
        EMPTY_FIELD.field = 'session';
        throw EMPTY_FIELD;
    }

    const ses = checksession(session);
    if (ses.type == 'user') {
        throw HAVENT_PERMISSION;
    }

    if (!buildid || buildid.length == 0) {
        EMPTY_FIELD.field = 'buildid';
        throw EMPTY_FIELD;
    }

    if (!status || buildStatuses.indexOf(status) == -1) {
        EMPTY_FIELD.field = 'status';
        throw EMPTY_FIELD;
    }

    let update = { $set: { status } };
    if (status === 'disapprove') {
        update = { $set: { status, disapprovereason } };
    }

    const query = { _id: new mongo.ObjectID(buildid) };

    const cb = dbo.collection('build');
    try {
        await cb.updateOne(query, update);
    } catch (e) {
        throw DATABASE_ERROR;
    }

}

// Function for getallbuilds
module.exports.getallbuilds = async(body) => {
    const { session } = body;

    let ses;
    let m = {};
    m['status'] = 'approve';
    if (session) {
        const query = { _id: session };

        const cs = dbo.collection("session");
        try {
            ses = await cs.findOne(query);
        } catch (e) {
            throw DATABASE_ERROR;
        }

        if (ses === null) throw SESSION_NOT_FOUND;

        if (ses.type == 'user') m['status'] = 'approve';
        if (ses.type == 'moderator' || ses.type == 'root') m = {};
    }

    const cb = dbo.collection('build');

    let builds;
    try {
        builds = await cb.find(m).toArray();
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (builds === null || builds.length === 0) throw COMP_NOT_FOUND;

    await new Promise(async(rslv, rej) => {
        for (let index = 0; index < builds.length; index++) {
            const build = builds[index];

            const cu = dbo.collection('account');
            let uid;

            try {
                uid = new mongo.ObjectID(build.owner);
            } catch (e) {
                throw BAD_BSON;
            }

            let user;
            try {
                user = await cu.findOne({ _id: uid });
            } catch (e) {
                console.log(e);
                throw DATABASE_ERROR;
            }
            if (user && user.length !== 0) {
                // builds[index]['user'] = {
                //     name: user.name,
                // };
                builds[index]['username'] = user.name;
            }

            for (const comp of comptypes) {
                let _id;

                try {
                    _id = new mongo.ObjectID(build[comp]);
                } catch (e) {
                    throw BAD_BSON;
                }

                let component;
                const cc = dbo.collection(comp);
                try {
                    component = await cc.findOne({ _id });
                } catch (e) {
                    console.log(e);
                    throw DATABASE_ERROR;
                }

                builds[index][comp] = component;
            }
        }
        rslv();
    });

    return builds;
}


// Function for add build to mongo
module.exports.addbuild = async(body) => {
    const { session, build, cords, description, name } = body;

    if (build === undefined) {
        EMPTY_FIELD.field = 'build';
        throw EMPTY_FIELD;
    }


    // Check required build fields 
    //   const field = checkBuild(build);
    //     if (field !== undefined && field.length > 0 ) {
    //       EMPTY_FIELD.field = field;
    //       throw EMPTY_FIELD;
    // }

    const ses = await checksession({ session });

    const compObj = {};
    var sum = 0;

    for (const comp in build) {
        let component = comp;
        // if (comp === 'drive') component = build[comp].type;
        if (comptypes.indexOf(component) === -1) {
            UNKNOWN_TYPE.msg += ' компонента';
            throw UNKNOWN_TYPE;
        }

        if (!build[comp] || !build[comp].length || build[comp].length == 0) {
            continue
        }

        const cc = dbo.collection(component);
        var cmp;

        const _id = new mongo.ObjectID(build[comp]);
        try {
            cmp = await cc.findOne({ _id });
        } catch (e) {
            throw DATABASE_ERROR;
        }

        if (cmp === null) {
            // COMP_NOT_FOUND.msg = component + ": " + COMP_NOT_FOUND.msg;
            throw COMP_NOT_FOUND;
        }

        sum += cmp.price;
        compObj[comp] = _id;
    }

    const status = '';
    const price = sum;
    const owner = ses.link;
    const _cords = (cords === undefined || cords.length == 0) ? [] : cords;
    let priceCord = 0;

    if (price > 20000 && price <= 30000) priceCord = 1
    if (price > 30000 && price <= 40000) priceCord = 2
    if (price > 40000 && price <= 50000) priceCord = 3
    if (price > 50000 && price <= 60000) priceCord = 4
    if (price > 60000 && price <= 70000) priceCord = 5
    if (price > 70000 && price <= 80000) priceCord = 6
    if (price > 80000 && price <= 90000) priceCord = 7
    if (price > 90000 && price <= 100000) priceCord = 8
    if (price > 100000 && price <= 110000) priceCord = 9
    if (price > 110000 && price <= 120000) priceCord = 10
    if (price > 120000 && price <= 130000) priceCord = 11
    if (price > 130000 && price <= 140000) priceCord = 12
    if (price > 140000 && price <= 150000) priceCord = 13
    if (price > 150000) priceCord = 14

    if (cords.length === 1) cords.push(priceCord)
    else if (cords && cords.length !== 0) cords.splice(1, 0, priceCord);

    const createDate = new Date();
    const lastEdit = new Date();
    const infoObj = { status, price, owner, createDate, description, name, lastEdit, cords: _cords };
    const result = Object.assign(infoObj, compObj);

    const cb = dbo.collection('build');

    try {
        await cb.insertOne(result);
    } catch (e) {
        throw BUILD_NOT_FOUND;
    }
};

module.exports.changepass = async(body) => {
    const { session, pass } = body;


    if (pass == undefined || pass.length == 0) {
        EMPTY_FIELD.field = 'pass';
        throw EMPTY_FIELD;
    }

    if (pass.length < 6 || pass.length > 16) throw BAD_PASS;

    let acc;

    const ses = await checksession({ session });

    const ca = dbo.collection("account");
    try {
        acc = await ca.findOne({ _id: ses.link });
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (acc == null) throw SESSION_NOT_FOUND;


    const password = crypto.pbkdf2Sync(pass, acc.salt, iterations, keylen, digest).toString();
    if (acc.password === password) throw IDENTIFY_PASS;

    const update = { $set: { password } };
    const query = { _id: acc._id };

    try {
        await ca.updateOne(query, update);
    } catch (e) {
        throw DATABASE_ERROR;
    }

}

module.exports.getPermissions = async(body) => {
    const { session } = body;

    if (!session || session.length == 0) {
        EMPTY_FIELD.field = 'session';
        throw EMPTY_FIELD;
    }

    const ses = await checksession({ session });

    if (!ses || ses._id.length == 0) throw DATABASE_ERROR;

    if (ses.type !== 'admin' && ses.type !== 'root') throw HAVENT_PERMISSION;

    const accTypes = [
        { type: 'user', name: 'Пользователь' },
        { type: 'moderator', name: 'Модератор' },
        { type: 'admin', name: 'Администратор' },
        { type: 'root', name: 'Root' }
    ];

    return accTypes;
}

module.exports.getAllAccounts = async(body) => {
    const { session } = body;

    if (!session || session.length == 0) {
        EMPTY_FIELD.field = 'session';
        throw EMPTY_FIELD;
    }

    const ses = await checksession({ session });

    if (!ses || ses._id.length == 0) throw DATABASE_ERROR;

    if (ses.type !== 'admin' && ses.type !== 'root') throw HAVENT_PERMISSION;

    const ca = dbo.collection('account');
    let accs;

    try {
        accs = await ca.find({}).toArray();
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (!accs) throw DATABASE_ERROR;

    let accounts = [];
    accs.forEach(function(acc) {
        const id = acc._id;
        const name = acc.name;
        const type = acc.type;
        const rustype = rusAccTypes[acc.type];
        accounts.push({ id, name, type, rustype });
    });

    return accounts;
}

module.exports.changeaccounttype = async(body) => {
    const { session, type, user } = body;

    if (type == undefined || type.length == 0) {
        EMPTY_FIELD.field = 'type';
        throw EMPTY_FIELD;
    }

    if (user == undefined || user.length == 0) {
        EMPTY_FIELD.field = 'user';
        throw EMPTY_FIELD;
    }

    if (accTypes.indexOf(type) === -1) {
        UNKNOWN_TYPE.msg += ' аккаунта';
        throw UNKNOWN_TYPE;
    }

    const ses = await checksession({ session });

    let acc;

    const ca = dbo.collection("account");
    try {
        acc = await ca.findOne({ _id: ses.link });
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (acc === null) throw SESSION_NOT_FOUND;

    if (accTypes.indexOf(acc.type) <= accTypes.indexOf(type)) throw HAVENT_PERMISSION;

    let accset;
    const userID = new mongo.ObjectID(user);
    try {
        accset = await ca.findOne({ _id: userID });
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (accset === null) throw SESSION_NOT_FOUND;


    const query = { _id: userID };
    const update = { $set: { type } };
    try {
        await ca.updateOne(query, update);
    } catch (e) {
        throw DATABASE_ERROR;
    }
}


// Add component by id
module.exports.getcomponent = async(body) => {
    const { type, comp } = body;

    if (comp == undefined || comp.length == 0) {
        EMPTY_FIELD.field = 'comp';
        throw EMPTY_FIELD;
    }

    if (type == undefined || type.length == 0) {
        EMPTY_FIELD.field = 'type';
        throw EMPTY_FIELD;
    }

    if (comptypes.indexOf(type) === -1) {
        UNKNOWN_TYPE.msg += ' компонента';
        throw UNKNOWN_TYPE;
    }

    const _id = new mongo.ObjectID(comp);
    let component;
    const cc = dbo.collection(type);

    try {
        component = await cc.findOne({ _id });
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (component === null) throw COMP_NOT_FOUND;

    return component;
}

// Get all components
module.exports.getallcomponents = async(body) => {
    const { type } = body;

    if (type == undefined || type.length == 0) {
        EMPTY_FIELD.field = 'type';
        throw EMPTY_FIELD;
    }

    if (comptypes.indexOf(type) === -1) {
        UNKNOWN_TYPE.msg += ' компонента';
        throw UNKNOWN_TYPE;
    }

    let components;
    const cc = dbo.collection(type);

    try {
        components = await cc.find({}).toArray();;
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (components === null) throw COMP_NOT_FOUND;

    return components;
}

module.exports.removeBuild = async(body) => {
    const { session, build } = body;

    if (!session || session.length == 0) {
        EMPTY_FIELD.field = 'session';
        throw EMPTY_FIELD;
    }

    const ses = await checksession({ session });

    if (!ses || ses._id.length == 0) throw DATABASE_ERROR;

    if (ses.type !== 'admin' && ses.type !== 'root') throw HAVENT_PERMISSION;

    const cb = dbo.collection('build');
    let _id = new mongo.ObjectID(build);

    try {
        await cb.deleteOne({ _id })
    } catch (e) {
        throw DATABASE_ERROR;
    }

}


const checksession = async(body) => {
    const { session } = body;

    if (session == undefined || session.length == 0) {
        EMPTY_FIELD.field = 'session';
        throw EMPTY_FIELD
    }

    let ses;
    const query = { _id: session };

    const cs = dbo.collection("session");
    try {
        ses = await cs.findOne(query);
    } catch (e) {
        throw DATABASE_ERROR;
    }

    if (ses === null) throw SESSION_NOT_FOUND;

    const lastUpdate = new Date();
    const update = { $set: { lastUpdate } };

    try {
        await cs.updateOne(query, update);
    } catch (e) {
        throw DATABASE_ERROR;
    }

    return ses;
}


module.exports.checksession = checksession;