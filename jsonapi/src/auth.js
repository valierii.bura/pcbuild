const { DATABASE_ERROR, INVALID_AUTH_PAIR, EMPTY_FIELD, LOGIN_USED, BAD_PASS } = require('./status.js');
const crypto = require('crypto');

/* Constant for signIn/signUp */
const keylen = 64,
    iterations = 10000,
    digest = 'sha256',
    enc = 'base64';

module.exports.signup = async(login, pass, name) => {
    if (login == undefined || login.length == 0) {
        EMPTY_FIELD.field = 'login';
        throw EMPTY_FIELD
    }

    if (pass == undefined || pass.length == 0) {
        EMPTY_FIELD.field = 'pass';
        throw EMPTY_FIELD
    }

    if (name == undefined || name.length == 0) {
        EMPTY_FIELD.field = 'name';
        throw EMPTY_FIELD
    }

    if (pass.length < 6 || pass.length > 16) {
        throw BAD_PASS;
    }

    let acc;

    const ca = dbo.collection("account");
    try {
        acc = await ca.findOne({ login });
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (acc !== null) {
        throw LOGIN_USED;
    }

    const salt = crypto.randomBytes(64).toString(enc);
    const password = crypto.pbkdf2Sync(pass, salt, iterations, keylen, digest).toString();
    const type = 'user';
    const registrationTime = new Date();
    const account = { login, salt, password, type, registrationTime, name };

    //Add account to DB

    try {
        await ca.insertOne(account);
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

};

module.exports.signin = async(login, pass) => {

    if (login == undefined || login.length == 0) {
        EMPTY_FIELD.field = 'login';
        throw EMPTY_FIELD
    }

    if (pass == undefined || pass.length == 0) {
        EMPTY_FIELD.field = 'pass';
        throw EMPTY_FIELD
    }

    const account = dbo.collection("account");

    let acc;
    try {
        acc = await account.findOne({ login });
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    if (acc == null) throw INVALID_AUTH_PAIR;

    const password = crypto.pbkdf2Sync(pass, acc.salt, iterations, keylen, digest).toString();

    if (acc.password !== password) throw INVALID_AUTH_PAIR;

    const link = acc._id;
    const type = acc.type;
    const lastUpdate = new Date();
    const _id = crypto
        .createHmac(
            digest,
            crypto.randomBytes(64).toString(enc),
        ).digest('hex');

    const session = { _id, link, type, lastUpdate };
    try {
        await dbo.collection("session").insertOne(session);
    } catch (e) {
        console.log(e);
        throw DATABASE_ERROR;
    }

    return { sid: _id };
};