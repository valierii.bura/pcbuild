const PORT = 8001;

const bodyParser = require('body-parser');
const express = require('express');
const api = express();
const mongo = require('mongodb');
const MongoClient = mongo.MongoClient;
const url = "mongodb://localhost:8002/";
const path = require('path');

const { UNKNOWN_METHOD, DONE } = require('./status.js');
const auth = require('./auth.js');
const jsonapi = require('./jsonapi.js');

api.use(bodyParser.json()); // support json encoded bodies
api.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

api.use(express.static(path.join(__dirname, 'views/public')));
api.get('/', (req, res) => {
    res.sendFile(__dirname + '/views/index.html');
    // res.sendFile(__dirname + '/views/script.js');
});

const main = (err, db) => {
    if (err) throw err;

    console.log("Connected to MongoDB");

    dbo = db.db("pcbuild");

    api.post('/auth', $auth(dbo));
    api.post('/api', $api(dbo));

    api.listen(PORT, () => console.log(`JSONAPI is started on port ${PORT}`));

    console.log('Done');
};

const $auth = db => async(req, res) => {
    const { login, pass, method, name } = req.body;

    try {
        const $method = auth[method];
        if ($method === undefined) throw UNKNOWN_METHOD;

        const result = await $method(login, pass, name);

        if (result === undefined) res.json(DONE);
        else res.json(result);

    } catch (e) {
        // LOGGING
        return res.json(e);
    }
};

const $api = db => async(req, res) => {
    // same as $auth(), just check sessions
    const { method } = req.body;

    try {
        const $method = jsonapi[method];
        if ($method === undefined) throw UNKNOWN_METHOD;

        const result = await $method(req.body);
        if (result === undefined) res.json(DONE);
        else res.json(result);

    } catch (e) {
        return res.json(e);
    }
};



MongoClient.connect(url, main);

// Get VideoCardById
/*
api.post('/api/getvideocard', (req, res) => {
    const id = new mongo.ObjectID(req.body.id);

    const query = { _id: id };

    dbo.collection("videocard").findOne(query, function(err, result) {
        if (err) throw err;
        res.json(result);
    });

}); */