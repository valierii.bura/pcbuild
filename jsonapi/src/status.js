module.exports = {
    UNKNOWN_METHOD: { err: true, msg: 'Неизвестный метод', id: 0 },
    INVALID_AUTH_PAIR: { err: true, msg: 'Неверная связка "логин/пароль"', id: 1 },
    DATABASE_ERROR: { err: true, msg: 'Ошибка базы данных', id: 2 },
    EMPTY_FIELD: { err: true, msg: 'Данное поле не может быть пустым', field: '', id: 3 },
    LOGIN_USED: { err: true, msg: 'Пользователь с таким логином уже существует', id: 4 },
    BAD_PASS: { err: true, msg: 'Пароль не удовлетворяет условиям', id: 5 },
    SESSION_NOT_FOUND: { err: true, msg: 'Сессия не найдена или истекла', id: 6 },
    IDENTIFY_PASS: { err: true, msg: 'Пароли не должны совпадать', id: 7 },
    UNKNOWN_TYPE: { err: true, msg: 'Неизвестный тип', id: 8 },
    HAVENT_PERMISSION: { err: true, msg: 'Недостаточно прав', id: 9 },
    COMP_NOT_FOUND: { err: true, msg: 'Компонент не найден', id: 10 },
    BAD_BSON: { err: true, msg: 'Невалидный идентификатор', id: 11 },
    BUILD_NOT_FOUND: { err: true, msg: 'Сборка не найдена', id: 12 },

    DONE: { id: -1 },
};