const setPermission = async(mainEl) => {

    mainEl.innerHTML = '';

    const { el, mount } = redom;

    const session = getCookie('pcbuilduser');
    let ses = await checksession(session);
    let method = 'getAllAccounts';
    const users = await $api(JSON.stringify({ session, method }));

    method = 'getPermissions';
    const permissions = await $api(JSON.stringify({ session, method }));

    const userByTypes = {};
    for (const user of users) {
        if (!userByTypes[user.rustype]) userByTypes[user.rustype] = [];
        userByTypes[user.rustype].push(user)
    }

    const mainColumns = el('div.columns');

    for (const userRuType in userByTypes) {
        if (userByTypes.hasOwnProperty(userRuType)) {

            const oneTypeUsers = userByTypes[userRuType];
            const column = el('div.column.col-5.col-mx-auto.accounts.my-2.py-2.px-2');
            const typeTitle = el('h3');
            typeTitle.innerText = userRuType;

            mount(column, typeTitle);
            mount(mainColumns, column);

            for (var user of oneTypeUsers) {

                if (user.id === ses.link) continue;

                const uName = el('span');
                uName.innerText = user.name;


                const permissionGroup = el('div.form-group');
                mount(permissionGroup, uName);
                const selectPermission = el('select.form-select.mb-2');
                const column2 = el('div.column');

                permissions.forEach(permission => {
                    const option = el('option', { value: permission.type });
                    option.innerText = permission.name;
                    mount(selectPermission, option);
                });

                selectPermission.value = user.type;

                mount(permissionGroup, selectPermission);
                mount(column2, permissionGroup);
                mount(column, column2);

                selectPermission.addEventListener('change', async() => {
                    const type = selectPermission.value;
                    const uid = user.id;
                    const method = 'changeaccounttype';
                    const body = JSON.stringify({ session, type, user: uid, method });
                    selectPermission.classList.add('load');
                    const res = await $api(body);
                    selectPermission.classList.remove('load');
                    if (res && res.id === -1) alert('Права доступа успешно изменены');
                    if (res && res.err) alert(res.msg);
                });
            }

        }
    }

    mount(mainEl, mainColumns);

    // console.log(userByTypes);

    // const columns = el('div.columns');

    // let usersOpt = [];

    // const column1 = el('div.column');
    // const userGroup = el('div.form-group');
    // const selectUser = el('select.form-select');

    // users.forEach( user => {
    //     const option = el('option', { value: user.id });
    //     option.innerText = user.name;
    //     mount(selectUser, option);

    //     usersOpt.push(option);
    // });

    // mount(userGroup,selectUser);
    // mount(column1, userGroup);
    // mount(columns, column1);

    // let permissionsOpt = [];

    // const permissionGroup = el('div.form-group');
    // const selectPermission = el('select.form-select');
    // const column2 = el('div.column');

    // permissions.forEach( permission => {
    //     const option = el('option', { value: permission.type });
    //     option.innerText = permission.name;
    //     mount(selectPermission, option);

    //     permissionsOpt.push(option);
    // });

    // mount(permissionGroup,selectPermission);
    // mount(column2, permissionGroup);
    // mount(columns, column2);

    // mount(mainEl, columns);

    // const acceptColumns = el('div.columns');
    // const acceptDiv = el('div.column.col-1.col-mx-auto.my-2');
    // const accept = el('button.btn.btn-success');
    // accept.innerText = 'Изменить права';

    // mount(acceptColumns, acceptDiv);
    // mount(acceptDiv, accept);
    // mount(mainEl, acceptColumns);

    // accept.addEventListener('click', async () => {
    //     const type = selectPermission.value;
    //     const user = selectUser.value;
    //     const method = 'changeaccounttype';
    //     const body = JSON.stringify( { session, type, user, method } );
    //     accept.classList.add('load');
    //     const res = await $api(body);
    //     accept.classList.remove('load');
    //     if ( res && res.id === -1 ) alert('Права доступа успешно изменены');
    //     if ( res && res.err ) alert(res.msg);
    // });

}