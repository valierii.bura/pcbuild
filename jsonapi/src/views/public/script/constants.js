const buildBtns = {
    // name: 'Автор',
    createDate: 'Дата',
}

// openInConstr: 'Открыть в конструкторе',
// approve: 'Одобрить',
// delete: 'Удалить',

var compEnToRU = {
    cpu: 'Процессор',
    hdd: 'HDD',
    ozu: 'Оперативная память',
    ssd: 'SSD',
    blockculler: 'Куллер для системного блока',
    block: 'Системный блок',
    cpuculler: 'Куллер для процессора',
    liquidcooling: 'Жидкое охлаждение',
    motherboard: 'Материнская плата',
    powersupply: 'Блок питания',
    videocard: 'Видеокарта',
}

var questions = [
    {
        label: 'Какого типа компьютер',
        questions: [
            'Самый простой',
            'Офисный',
            'Средний',
            'Игровой', 
            'Монтаж видео',
            'Работа с анимацией',
            'Программирование',
            'Работа с большими данными',
            'Универссальный компьютер',
        ],
        firldToVal: {
            'Самый простой': 'simple',
            'Офисный': 'simple',
            'Средний': 'simple',
            'Игровой': 'simple', 
            'Работа с графикой': 'simple',
        },
        labelName:'type',
    },
    {
        label: 'Ценовой диапозон',
        questions: [
            '20 000  - 30 000 руб.',
            '30 000  - 40 000 руб.',
            '40 000  - 50 000 руб.',
            '50 000  - 60 000 руб.',
            '60 000  - 70 000 руб.',
            '70 000  - 80 000 руб.',
            '80 000  - 90 000 руб.',
            '90 000  - 100 000 руб.',
            '100 000 - 110 000 руб.',
            '110 000 - 120 000 руб.',
            '120 000 - 130 000 руб.',
            '130 000 - 140 000 руб.',
            '140 000 - 150 000 руб.',
        ],
        firldToVal: {
            '10 000': 'simple',
            '20 000': 'simple',
            '30 000': 'simple',
            '40 000': 'simple', 
            '50 000': 'simple',
            '60 000': 'simple',
            '70 000': 'simple',
        },
        labelName:'price',
    }
]

const emptyBlock = `
<div class="empty">
  <div class="empty-icon">
    <i class="icon icon-people"></i>
  </div>
  <p class="empty-title h5">You have no new messages</p>
  <p class="empty-subtitle">Click the button to start a conversation.</p>
  <div class="empty-action">
    <button class="btn btn-primary">Send a message</button>
  </div>
</div>`;