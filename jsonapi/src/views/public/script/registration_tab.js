const registration = (mainEl) => {

    mainEl.innerHTML = '';
    mainEl.classList.add('columns');

    const { el, mount } = redom;

    // Create Containner
    columns = el('div.column.py-2');
    mount(mainEl, columns);
    container = el('div.col-4.col-mx-auto');
    mount(columns, container);

    if (getCookie('pcbuilduser') && getCookie('pcbuilduser').length > 0) {
        //Create GroupSpace
        const groupSpace = el(el('div.col-4.col-mx-auto'), el('div.input-group.space'));
        mount(container, groupSpace);

        //Create Exit Button
        const btnExit = el('button.btn.btn-error.mx-2');
        btnExit.innerText = 'Выйти';
        mount(groupSpace, btnExit);

        btnExit.addEventListener('click', () => exit());
        st = { columns, container, groupSpace, btnExit };
        return;
    }

    const divider = el('div.divider-vert', { 'data-content': 'ИЛИ' });
    mount(mainEl, divider);
    //Create GroupSpace
    const groupSpace = el('div.input-group.space');
    mount(container, groupSpace);
    // Create input name
    const loginInp = el('input.form-input.my-2');
    loginInp.placeholder = 'Login';
    mount(groupSpace, loginInp);

    //Create GroupSpace2
    const groupSpace2 = el('div.input-group.space');
    mount(container, groupSpace2);

    // Create password name
    const passInp = el('input.my-2.form-input', { type: 'password', placeholder: 'Password' });
    mount(groupSpace2, passInp)


    //Create GroupSpace
    const groupSpace3 = el(el('div.col-6.col-mx-auto'), el('div.input-group.space'));
    mount(container, groupSpace3);

    //Create Auth Button
    const btnAuth = el('button.btn.btn-primary.mx-2', { style: { width: '100%' } });
    btnAuth.innerText = 'Вход';
    mount(groupSpace3, btnAuth);

    const st1 = { columns, container, groupSpace, loginInp, groupSpace2, passInp, btnAuth };
    btnAuth.addEventListener('click', () => $auth({ loginInp, passInp }));

    // FINISH AUTH BLOCK

    // START REGISTRATION BLOCK

    // Create Containner
    const columnsReg = el('div.column.py-2.my-2');
    mount(mainEl, columnsReg);

    const containerReg = el('div.col-4.col-mx-auto');
    mount(columnsReg, containerReg);

    //Create GroupSpace
    const groupSpaceReg = el('div.input-group.space');
    mount(containerReg, groupSpaceReg);

    // Create input name
    const loginInpReg = el('input.form-input.my-2');
    loginInpReg.placeholder = 'Login';
    mount(groupSpaceReg, loginInpReg);

    //Create GroupSpaceReg2
    const groupSpaceReg2 = el('div.input-group.space');
    mount(containerReg, groupSpaceReg2);

    // Create  name
    const nameInp = el('input.my-2.form-input', { placeholder: 'Name' });
    mount(groupSpaceReg2, nameInp)

    //Create GroupSpaceReg
    const groupSpaceReg3 = el('div.input-group.space');
    mount(containerReg, groupSpaceReg3);

    // Create password name
    const passInpReg = el('input.my-2.form-input', { type: 'password', placeholder: 'Password' });
    mount(groupSpaceReg3, passInpReg)

    //Create GroupSpaceReg
    const groupSpaceReg4 = el(el('div.col-6.col-mx-auto'), el('div.input-group.space'));
    mount(containerReg, groupSpaceReg4);

    //Create SignUpBtn Button
    const btnReg = el('button.btn.btn-success.mx-2', { style: { width: '100%' } });
    btnReg.innerText = 'Регистрация';
    mount(groupSpaceReg4, btnReg);

    //FINISH REGISTRATION BLOCK

    const st2 = { columnsReg, containerReg, groupSpaceReg, loginInpReg, groupSpaceReg2, passInpReg, btnAuth, btnReg };

    // 
    btnReg.addEventListener('click', () => $reg({ loginInpReg, passInpReg, nameInp }));

    st = Object.assign(st1, st2);

    return st;
}