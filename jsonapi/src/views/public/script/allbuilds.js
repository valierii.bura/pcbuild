const $getallbuilds = async(mainEl, allbuilds) => {

    mainEl.innerHTML = '';

    const btnsArr = [{
            name: 'Название сборки',
            field: 'name',
        },
        {
            name: 'Автор сборки',
            field: 'owner',
        },
        {
            name: 'Цена',
            field: 'price',
        },
        {
            name: 'Дата',
            field: 'createDate',
        },
    ];

    const btnsFilters = [{
            name: 'Название сборки',
            field: 'name',
            type: 'text',
        },
        {
            name: 'Автор сборки',
            field: 'username',
            type: 'text',
        },
    ];

    const statusBtn = {
        name: 'Статус',
        field: 'status'
    }

    if (!allbuilds || allbuilds.length === 0) {
        mainEl.innerHTML = emptyBlock;
        return;
    }

    const session = getCookie('pcbuilduser');
    const ses = await checksession(session);

    if (ses && ses._id && ses._id.length > 0 && ses.type !== 'user') btnsArr.push(statusBtn);

    const components = await $api(JSON.stringify({ method: 'components' }));
    if (!components || components.length === 0) return;


    const { el, mount } = redom;
    const wrapper = el('div.allbuilds.columns');
    const sorts = el('div.allbuilds.columns');
    const filters = el('div.allbuilds.columns');
    mount(mainEl, sorts);
    mount(mainEl, filters);
    createSorts(mainEl, btnsArr, allbuilds, components, wrapper, sorts);
    createFilters(filters, btnsFilters, allbuilds, mainEl, components, wrapper, sorts, btnsArr);
    mount(mainEl, wrapper);

    showBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts, filters);
}

const banevent = (buildid, session, status, disapprovereason) => {
    const method = 'approvebuild';
    return $api(JSON.stringify({ method, session, buildid, status, disapprovereason }));
}

const removeEvent = (build, session) => {
    const method = 'removeBuild';
    return $api(JSON.stringify({ method, session, build }));
}

const showBuilds = async(mainEl, allbuilds, components, wrapper, btnsArr, sorts, filters) => {
    wrapper.innerHTML = '';
    const { el, mount } = redom;

    const _ses = getCookie('pcbuilduser');
    if (_ses.length > 0) {
        const getMyBtn = el('button.btn');
        getMyBtn.innerText = 'Показать только мои сборки';
        getMyBtn.addEventListener('click', () => getMyBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts, filters));
        mount(wrapper, getMyBtn);
    }

    for (const build of allbuilds) {

        const openBuild = {
            cpu: {},
            hdd: {},
            ozu: {},
            ssd: {},
            blockculler: {},
            block: {},
            cpuculler: {},
            liquidcooling: {},
            motherboard: {},
            powersupply: {},
            videocard: {},
        };

        const bb = el('div.builds');
        const btnColumns = el('div.columns');

        mount(bb, btnColumns);
        const btnColumn = el('div.column.col-12.my-2');
        mount(btnColumns, btnColumn);

        const info = el('div.btn-group.btn-group-block');

        const d = new Date(build.createDate);
        const y = d.getFullYear();
        const _m = d.getMonth() < 12 ? d.getMonth() + 1 : d.getMonth();
        const m = _m.toString().length == 1 ? `0${_m}` : _m;
        const dd = d.getDate() < 10 ? `0${d.getDate()}` : d.getDate();
        const fullDateStart = `${dd}.${m}.${y}`;
        if (build.name) createBuildBtn(info, 'Название', build.name, true);
        createBuildBtn(info, 'Дата создания', fullDateStart, true);

        createBuildBtn(info, 'Автор', build.username, true);

        const openInConstr = el('button.btn.btn-success');
        openInConstr.innerText = 'Открыть в конструкторе';
        mount(info, openInConstr);

        const session = getCookie('pcbuilduser');
        if (session && session.length > 0) {
            const ses = await checksession(session);

            if (ses.type != 'user') {

                if (build.status == 'disapprove') {
                    const label = el('span.text-error');
                    label.innerText = 'Забанен, Причина: ' + build.disapprovereason;
                    mount(bb, label);
                }
                if (build.status == 'approve') {
                    const label = el('span.text-success');
                    label.innerText = 'Одобрен';
                    mount(bb, label);
                }
                if (build.status == '') {
                    const label = el('span.text-warning');
                    label.innerText = 'На модерации';
                    mount(bb, label);
                }

                if (build.status == '' || build.status == 'approve') {
                    const disApproveBen = el('button.btn.btn-error.tooltip', { style: 'width: 50px', 'data-tooltip': 'Забанить' });
                    const ic = el('i.icon.icon-cross');
                    mount(disApproveBen, ic);
                    mount(info, disApproveBen);

                    const inp = el('input.form-input.column.col-2');
                    mount(info, inp);
                    disApproveBen.addEventListener('click', async() => {
                        await banevent(build._id, session, 'disapprove', inp.value);
                        const method = 'getallbuilds';
                        const body = JSON.stringify({ session, method });
                        const allbuilds = await $api(body);
                        createSorts(mainEl, btnsArr, allbuilds, components, wrapper, sorts);
                        mount(mainEl, wrapper);
                        showBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts), filters;
                    });
                }

                if (build.status == '' || build.status == 'disapprove') {
                    const approveBen = el('button.btn.btn-success.tooltip', { style: 'width: 50px', 'data-tooltip': 'Одобрить' });
                    const ic = el('i.icon.icon-check');
                    mount(approveBen, ic);
                    mount(info, approveBen);

                    approveBen.addEventListener('click', async() => {
                        await banevent(build._id, session, 'approve', '');
                        const method = 'getallbuilds';
                        const body = JSON.stringify({ session, method });
                        const allbuilds = await $api(body);
                        createSorts(mainEl, btnsArr, allbuilds, components, wrapper, sorts);
                        mount(mainEl, wrapper);
                        showBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts), filters;
                    });
                }

                if (ses.type == 'admin' || ses.type == 'root') {
                    const removeBtn = el('button.btn.btn-error.tooltip', { style: 'width: 50px', 'data-tooltip': 'Удалить' });
                    const ic = el('i.icon.icon-delete');
                    mount(removeBtn, ic);
                    mount(info, removeBtn);

                    removeBtn.addEventListener('click', async() => {
                        if (!confirm('Вы уверены что хотите удалить сборку?')) return
                        await removeEvent(build._id, session);
                        const method = 'getallbuilds';
                        const body = JSON.stringify({ session, method });
                        const allbuilds = await $api(body);
                        createSorts(mainEl, btnsArr, allbuilds, components, wrapper, sorts);
                        mount(mainEl, wrapper);
                        showBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts), filters;
                    });
                }

            }
        }

        mount(btnColumn, info);

        const columns = el('div.columns');
        mount(bb, columns);

        for (const comp of components) {

            if (!build[comp] && build[comp] === null) continue;
            const imgsrc = build[comp].imgurl ? build[comp].imgurl : '';
            title = build[comp].model ? build[comp].model : '',
                desc = build[comp].firm ? build[comp].firm : '',
                text = build[comp].description ? build[comp].description : '',
                price = build[comp].price ? build[comp].price : '',
                btnText = compEnToRU[comp],
                btnClbk = () => {},
                cardWidth = 250;
            openBuild[comp] = build[comp]._id;
            const column = el('div.column.col-2.my-2');

            const genCard = {
                mainEl: column,
                imgsrc,
                title,
                desc,
                text,
                btnText,
                btnClbk,
                cardWidth,
                price,
            }

            createcard(genCard);

            mount(columns, column);
        }

        const descTitle = el('h4');
        descTitle.innerText = 'Описание: ';
        const cordVal = el('span.label.label-primary.py-2');
        cordVal.innerText = questions[0].questions[build.cords[0]];
        const priceTitle = el('h5');
        priceTitle.innerText = `Общая стоимость: ${build.price} руб.`;

        const descriptionSpan = el('span');
        descriptionSpan.innerText = build.description ? build.description : '';

        if (build.description) {
            mount(bb, descTitle);
            mount(bb, descriptionSpan);
        }

        mount(bb, cordVal);

        if (build.price) {
            mount(bb, priceTitle);
        }

        mount(wrapper, bb);
        openInConstr.addEventListener('click', () => openBuildInConstructor(openBuild, mainEl));
    }
}

const openBuildInConstructor = (openBuild, mainEl) => {
    selectBuild = Object.assign(openBuild);
    contruct(mainEl);
    aTabs[prevTab].classList.remove('active');
    aTabs[1].classList.add('active');
    prevTab = 1;
};

const createSorts = (mainEl, btnsArr, allbuilds, components, wrapper, sorts, filters) => {
    sorts.innerHTML = '';
    const { el, mount } = redom;
    const field = btnsArr[0].field;
    const count = 0;

    const span = el('span', { style: 'margin-top: 7px; margin-right: 16px;' });
    span.innerText = 'Сортировать по: ';
    mount(sorts, span);

    var prev = { field, count };
    for (const btn of btnsArr) {
        const btnEl = el('button.btn.filters-btn');
        btnEl.innerText = btn.name;
        const field = btn.field;
        btnEl.addEventListener('click',
            () => {
                prev = sortClbk(field, allbuilds, mainEl, components, prev, wrapper, btnsArr, sorts, filters);
                const allbtns = mainEl.querySelectorAll('.filters-btn');
                for (const bt of allbtns) {
                    bt.classList.remove('btn-primary');
                }
                btnEl.classList.add('btn-primary');
            });
        mount(sorts, btnEl);
    }
}

const createFilters = (filters, btnsFilters, allbuilds, mainEl, components, wrapper, sorts, btnsArr) => {
    const { el, mount } = redom;

    const span = el('span', { style: 'margin-top: 17px; margin-right: 16px;' });
    span.innerText = 'Фильтровать по: ';
    mount(filters, span);

    let filterBtns = [];

    for (const btn of btnsFilters) {
        //Create GroupSpace
        const groupSpace = el('div.input-group.space');
        mount(filters, groupSpace);

        // Create input name
        const filter = el('input.form-input.my-2');
        filter.placeholder = btn.name;
        mount(groupSpace, filter);

        const field = btn.field;
        filterBtns.push({ filter, field });
    }

    for (let index = 0; index < filterBtns.length; index++) {
        filterBtns[index].filter.addEventListener('change', () => filterClbk(filterBtns));
    }

    const filterClbk = (filterBtns) => {
        let _allbuilds = allbuilds;

        filterBtns.forEach(filterBtn => {
            _allbuilds = _allbuilds.filter(build => {
                const filterValue = filterBtn.filter.value.toLowerCase() || '';
                return build[filterBtn.field].toLowerCase().indexOf(filterValue) !== -1;
            });
        });

        showBuilds(mainEl, _allbuilds, components, wrapper, btnsArr, sorts, filters);
    };

}

const getMyBuilds = async(mainEl, allbuilds, components, wrapper, btnsArr, sorts, filters) => {
    const session = getCookie('pcbuilduser');
    let ses = await checksession(session);

    if (!ses || ses._id.length === 0) return;

    let _allbuilds = allbuilds;

    _allbuilds = _allbuilds.filter(build => {
        return build.owner === ses.link;
    });

    showBuilds(mainEl, _allbuilds, components, wrapper, btnsArr, sorts, filters);

}

const sortClbk = (field, allbuilds, mainEl, components, prev, wrapper, btnsArr, sorts, filters) => {

    allbuilds = allbuilds.sort(SortByField(field));

    if (prev.field == field) prev.count++;

    if (prev.field && prev.field == field && prev.count != 0 && prev.count % 2 == 0) {
        allbuilds = allbuilds.sort(SortByFieldDesc(field));
        prev.count = 0;
    } else if (prev.field != field) {
        prev.field = field;
        prev.count = 1;
    }

    showBuilds(mainEl, allbuilds, components, wrapper, btnsArr, sorts, filters);

    return prev;
}


//     allbuilds.filter(build => {
//     return build.name.toLowerCase().indexOf(this.search.toLowerCase()) !== -1;
// });