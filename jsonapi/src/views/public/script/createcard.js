/*
 * createcard get folowed params
 * mainEl - parent element for appendChild
 * imgsrc - imgsrc
 * tittle - tittle
 * desc - description
 * text - text
 * btnText - btnText 
 * btnClbk - clbk for button click
 * cardWidth - width of card
 */


const createcard = (genCard) => {
    const {
        mainEl,
        imgsrc,
        title,
        desc,
        text,
        btnText,
        btnClbk,
        cardWidth,
        price,
    } = genCard;

    const { el, mount } = redom;

    // const card = el('div.card', { style: { width: cardWidth + 'px' } } );
    const card = el('div.card.p-2.component', { style: `height: 100%; cursor: pointer;` });
    // const card = el('div.card',  { style: `height: 100%; width: ${cardWidth}` } );

    const cardImg = el('div.card-image');
    mount(card, cardImg);

    const img = el('img.img-responsive', { src: imgsrc });
    mount(cardImg, img);

    const cardHeader = el('div.card-header')
    const h5 = el('div.card-title.h5');
    h5.innerText = title;

    const subtitle = el('div.card-subtitle.text-gray');
    subtitle.innerText = desc;

    mount(cardHeader, h5);
    mount(cardHeader, subtitle);
    mount(card, cardHeader);

    const body = el('div.card-body');
    body.innerText = text;
    mount(card, body);

    const footer = el('div.card-footer');
    const btn = el('button.btn.btn-primary');
    btn.innerText = btnText;

    const priceh5 = el('div.card-title.h5');
    priceh5.innerText = `Цена: ${price} руб.`;
    mount(footer, priceh5);

    btn.addEventListener('click', btnClbk);

    mount(footer, btn);
    mount(card, footer);

    mount(mainEl, card);

    return { img, h5, body, priceh5, subtitle, card }
}

const createBuildBtn = (mainEl, name, value, disabled) => {

    const { el, mount } = redom;

    const btn = el('button.btn.btn-no-opacity');
    if (disabled) btn.classList.add('disabled');
    btn.innerText = `${name}:${value}`;

    mount(mainEl, btn);
}