//  Create question for build
// quests = [ { label: 'How are you?', questions: ['','','','',''] } ]


const questcreate = (parent, questions) => {

    const { el, mount } = redom;
    const chks = [];

    const columns = el('div.columns');
    const maincol = el('div.column.col-6.col-mx-auto');

    mount(parent, columns);
    mount(columns, maincol);

    for (const question of questions) {
        // const column = el('div.column.col-4.p-2');
        // mount(columns, column);

        const formH = el('form.form-horizontal.column.col-6.p-2.col-mx-auto');
        mount(maincol, formH);

        const h3 = el('h3.text-center');
        h3.innerText = question.label;
        mount(formH, h3);

        const formG = el('div.form-group.p-2');
        mount(formH, formG);

        const qq = {};
        let ind = 0;
        for (const quest of question.questions) {

            const div = el('div.column.col-6.mx-auto');
            const labelD = el('label.form-radio');
            const inp = el('input', { type: "radio", name: question.labelName, value: ind });
            const i = el('i.form-icon');

            mount(formG, div);
            mount(div, labelD);
            mount(labelD, inp);
            mount(labelD, i);

            if (ind === 0) {
                inp.setAttribute("checked", "");
            }

            qq[ind] = inp;

            labelD.innerHTML += ' ' + quest;
            ind++;

        }

        const chk = { parent, formH, formG, h3, qq };

        chks.push(chk);
    }

    return chks;
}