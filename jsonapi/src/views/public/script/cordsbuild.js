const build = (mainEl) => {

    mainEl.innerHTML = '';

    const { el, mount } = redom;

    questcreate(mainEl, questions);

    const send = el('button.btn.btn-success.form-inline.float-right');
    send.innerText = 'Подобрать ПК';
    mount(mainEl, send);

    const inpsType = mainEl.querySelectorAll('[name="type"]');
    const inpsPrice = mainEl.querySelectorAll('[name="price"]');
    setChecked(inpsType);
    setChecked(inpsPrice);

    send.addEventListener('click', async() => {
        const result = mainEl.querySelectorAll('[checked]');
        let cords = [];
        for (const res of result) {
            cords.push(parseFloat(res.value));
        }

        // { session, build, cords, description  }
        const method = 'buildbycords';

        const builds = await $api(JSON.stringify({ method, cords }));

        aTabs[prevTab].classList.remove('active');
        aTabs[2].classList.add('active');
        prevTab = 1;

        $getallbuilds(mainEl, builds);
    });
}

function setChecked(inpsType) {
    for (const inp of inpsType) {
        inp.addEventListener('change', () => {
            for (const inp2 of inpsType) {
                inp2.removeAttribute("checked");
            }
            inp.setAttribute("checked", "");
        });
    };
}