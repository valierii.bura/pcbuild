var selectBuild = {};
var buildDescription = '';
var sum = 0;

const user = {
	sid: '',
	name: ''
}

var modalObj = {};

var allcomps = {
    cpu: {},
    hdd: {},
    ozu: {},
    ssd: {},
    blockculler: {},
    block: {},
    cpuculler: {},
    liquidcooling: {},
    motherboard: {},
    powersupply: {},
    videocard: {},
};

const SortByField = field => ((a, b) => {
    if ( typeof a[field] == 'number' ) {
        if (a[field] < b[field]) return -1;
        if (a[field] > b[field]) return 1;
        return 0;
    } else {
        if (a[field].toLowerCase() < b[field].toLowerCase()) return -1;
        if (a[field].toLowerCase() > b[field].toLowerCase()) return 1;
        return 0;
    }
});

const SortByFieldDesc = field => ((a, b) => {
    if ( typeof a[field] == 'number' ) {
        if (a[field] > b[field]) return -1;
        if (a[field] < b[field]) return 1;
        return 0;
    } else {
        if (a[field].toLowerCase() > b[field].toLowerCase()) return -1;
        if (a[field].toLowerCase() < b[field].toLowerCase()) return 1;
        return 0;
    }
});

const $api = async (body, api = 'api') => {
    const headers = { 'content-type': 'application/json' };
    
	return new Promise((resolve, reject) => {
        window.fetch(`http://localhost:8001/${api}`, { method: 'POST', body, headers })
        .then(
            response => {
                if (!response.ok) {
                    response.json().then(json => {
                        reject(json);
                    });
                } else response.json().then(json => resolve(json));
            },
            error => console.log(error)
			);
        });
    }
    

const $auth = async (data) => {
	const { loginInp, passInp } = data;

	if ((!loginInp || !passInp)) return

	const login = loginInp.value.trim(),
		pass = passInp.value.trim(),
		method = 'signin',
		body = JSON.stringify({ login, pass, method }),
		{ sid } = await $api(body, 'auth');
	
	if ( !sid || sid.length === 0 ) return

	document.cookie = `pcbuilduser=${sid}`;
	console.log(sid);
	alert('Вход выполнен успешно');
	location.reload();

	loginInp.value = '';
	passInp.value = '';
}

const $reg = async (data) => {
	const { loginInpReg, passInpReg, nameInp } = data;

	if ((!loginInpReg || !passInpReg || !nameInp )) return

	const login = loginInpReg.value.trim(),
		pass = passInpReg.value.trim(),
		name = nameInp.value.trim(),
		method = 'signup',
		body = JSON.stringify({ login, pass, name,  method });

	await $api(body, 'auth');

	alert('Регистрация выполнена успешно, авторизуйтесь');
	location.reload();
}

const exit = () => {
	document.cookie = `pcbuilduser=`;
	location.reload();
};

const checksession = (session) => {
    const method = 'checksession',
	body = JSON.stringify( { session, method } );
	
	return $api(body);
}

const getCookie = name => {
	const value = "; " + document.cookie;
	const parts = value.split("; " + name + "=");
	if (parts.length == 2) return parts.pop().split(";").shift();
}

// Check session
(async () => {
    const modal = document.querySelector('.modal-lg');
    const modalTitle = modal.querySelector('.modal-title');
    const modalContent = modal.querySelector('.content');
    const modalFooter = modal.querySelector('.modal-footer');
    const modalClose = modal.querySelector('#modalclose');
    const modalCompName = modal.querySelector('.compname');
    modalClose.addEventListener('click', () => {
        modal.classList.remove('active');
    })

    modalObj = {modal, modalTitle, modalContent, modalFooter, modalClose, modalCompName };

    const components = await $api( JSON.stringify({ method: 'components' }) );

    for (const comp of components) {
        selectBuild[comp] = ''; 
        const type = comp;
        const method = 'getallcomponents';

        const body = JSON.stringify({ method, type });
        const _comps = await $api(body);
        for (const _comp of _comps) {
            allcomps[comp][_comp._id] = _comp;
        }
    }
    
    console.log(allcomps);
	const session = getCookie('pcbuilduser');
	console.log(session);
	if ( session == undefined ) return;

    const ses = await checksession(session);
    if ( !ses ) return;

	user.sid = ses._id;
	user.type = ses.type;
    console.log(user);
})();
