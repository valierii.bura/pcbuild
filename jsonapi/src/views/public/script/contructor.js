// Function for generate construct page
var prevSelect;
var select = {
    cpu: {},
    hdd: {},
    ozu: {},
    ssd: {},
    blockculler: {},
    block: {},
    cpuculler: {},
    liquidcooling: {},
    motherboard: {},
    powersupply: {},
    videocard: {},
};
var cards = {};

const contruct = async(mainEl) => {

    mainEl.innerHTML = '';
    const { el, mount } = redom;

    // const bb = el('div.build');

    const columns = el('div.columns');
    mount(mainEl, columns);

    const components = await $api(JSON.stringify({ method: 'components' }));

    const sumPriceP = el('p');
    sumPriceP.innerText = `Общая сумма сборки: ${sum} руб.`;

    const ell = {};
    for (const comp in selectBuild) {
        if (components.indexOf(comp) == -1) continue;

        if (selectBuild.hasOwnProperty(comp)) {
            const compid = selectBuild[comp];

            if (compid && compid.length > 0) {
                const imgsrc = allcomps[comp][compid].imgurl ? allcomps[comp][compid].imgurl : '';
                title = allcomps[comp][compid].model ? allcomps[comp][compid].model : 'Модель',
                    desc = allcomps[comp][compid].firm ? allcomps[comp][compid].firm : 'Фирма',
                    text = allcomps[comp][compid].description ? allcomps[comp][compid].description : 'Описание',
                    price = allcomps[comp][compid].price ? allcomps[comp][compid].price : '0',
                    btnText = compEnToRU[comp],
                    btnClbk = () => {
                        clbkBtn(comp, compEnToRU[comp], sumPriceP)
                    },
                    cardWidth = '25%';

                addGenCard(imgsrc, el, comp, sumPriceP, mount, columns);

                if (!allcomps[comp][compid]) continue;

                ell[comp] = allcomps[comp][compid];
                ell[comp].id = compid;

            } else {
                const imgsrc = allcomps[comp].imgurl ? allcomps[comp].imgurl : '';
                title = allcomps[comp].model ? allcomps[comp].model : 'Модель',
                    desc = allcomps[comp].firm ? allcomps[comp].firm : 'Фирма',
                    text = allcomps[comp].description ? allcomps[comp].description : 'Описание',
                    price = allcomps[comp].price ? allcomps[comp].price : '0',
                    btnText = compEnToRU[comp],
                    btnClbk = () => {
                        clbkBtn(comp, compEnToRU[comp], sumPriceP);
                    },
                    cardWidth = '25%';


                addGenCard(imgsrc, el, comp, sumPriceP, mount, columns);
            }

        }
    }

    changeComp(ell, sumPriceP);

    const formcolumns = el('div.columns');
    const formcolumn = el('div.column');
    const fgroup = el('div.form-group');
    const label = el('div.form-label.form-inline');

    //Create GroupSpace
    const groupSpace = el('div.input-group.space');
    // Create input name
    const nameInp = el('input.form-input.my-2');
    nameInp.placeholder = 'Build name';

    const descInp = el('textarea.form-input.form-inline', { placeholder: 'Description' });
    const send = el('button.btn.btn-success.form-inline.float-right');
    send.innerText = 'Добавить сборку';
    descInp.value = buildDescription;

    mount(mainEl, sumPriceP);

    mount(mainEl, formcolumns);
    mount(formcolumns, formcolumn);
    mount(formcolumn, fgroup);
    mount(fgroup, groupSpace);
    mount(groupSpace, nameInp);
    mount(fgroup, label);
    mount(fgroup, descInp);

    let quests = [];
    for (const quest of questions) {
        quests.push(quest);
    }

    quests.pop();

    const elll = questcreate(mainEl, quests);

    descInp.addEventListener('change', () => {
        buildDescription = descInp.value;
    })

    mount(mainEl, send);

    const inpsType = document.querySelectorAll('[name="type"]');
    const inpsPrice = document.querySelectorAll('[name="price"]');
    setChecked(inpsType);
    setChecked(inpsPrice);

    send.addEventListener('click', async() => {
        const result = document.querySelectorAll('[checked]');
        const cords = [];

        for (const res of result) {
            cords.push(parseFloat(res.value));
        }

        // { session, build, cords, description  }
        const method = 'addbuild';
        const session = getCookie('pcbuilduser');
        const build = {};
        const description = buildDescription;
        const name = nameInp.value;
        const components = await $api(JSON.stringify({ method: 'components' }));
        for (const comp of components) {
            build[comp] = selectBuild[comp];
        }

        await $api(JSON.stringify({ method, session, description, cords, build, name }));
    });
}


const clbkBtn = (type, title, sumEl) => {

    const { el, mount } = redom;
    modalObj.modalTitle.innerText = title;
    modalObj.modalContent.innerHTML = '';
    modalObj.modalFooter.innerHTML = '';

    const active = modalObj.modal.className.indexOf('active') !== -1;

    if (active) {
        modalObj.modal.classList.remove('active');
        return;
    } else modalObj.modal.classList.add('active');

    const comps = [];
    for (const id in allcomps[type]) {
        if (allcomps[type].hasOwnProperty(id)) {
            const element = allcomps[type][id];
            comps.push(element);
        }
    }

    changeModalContent(type, modalObj, comps);
    modalObj.modalCompName.addEventListener('change', () => changeClbk(type, modalObj, comps));

    const btnsuc = el('button.btn.btn-success');
    btnsuc.innerText = 'Принять';
    const btnDel = el('button.btn.btn-error.mx-2');
    btnDel.innerText = 'Удалить';

    btnsuc.addEventListener('click', () => {
        // {cardImg, cardHeader, body, priceh5}
        modalObj.modal.classList.remove('active');
        if (!select[type] || !select[type].desc || !select[type].name || !select[type].firm) return;
        cards[type].img.src = select[type].imgurl;
        cards[type].h5.innerText = select[type].name.innerText;
        cards[type].body.innerText = select[type].desc.innerText;
        cards[type].subtitle.innerText = select[type].firm.innerText;
        cards[type].priceh5.innerText = select[type].price.innerText;
        changeComp(select, sumEl);
    });

    btnDel.addEventListener('click', () => {
        // {cardImg, cardHeader, body, priceh5}
        modalObj.modal.classList.remove('active');

        cards[type].img.src = '';
        cards[type].h5.innerText = 'Модель'
        cards[type].body.innerText = 'Описание'
        cards[type].subtitle.innerText = 'Фирма'
        cards[type].priceh5.innerText = 'Цена: 0 руб.'
        select[type] = {};
        changeComp(select, sumEl);
    });


    mount(modalObj.modalFooter, btnDel);
    mount(modalObj.modalFooter, btnsuc);

}

const changeClbk = (type, modalObj, comps) => {
    comps = comps.filter(comp => {
        return comp.title.toLowerCase().indexOf(modalObj.modalCompName.value.toLowerCase()) !== -1;
    });
    changeModalContent(type, modalObj, comps);
}

const changeComp = async(selbuild, sumEl) => {
    let _sum = 0;

    const components = await $api(JSON.stringify({ method: 'components' }));
    if (!components || components.length === 0) return;

    for (const comp of components) {

        if (!selbuild[comp]) continue;
        const id = selbuild[comp].id;
        const _comp = allcomps[comp][id];

        if (!_comp) continue;
        _sum += _comp.price;
    }
    sumEl.innerText = `Общая сумма сборки: ${_sum} руб.`;
}

function addGenCard(imgsrc, el, comp, sumPriceP, mount, columns) {
    const genCard = {
        mainEl: column,
        imgsrc,
        title,
        desc,
        text,
        btnText,
        btnClbk,
        cardWidth,
        price,
    };
    const column = el('div.column.col-3.my-2');
    cards[comp] = createcard(genCard);
    const _card = cards[comp];
    const { card } = _card;
    card.addEventListener('click', () => clbkBtn(comp, compEnToRU[comp], sumPriceP));
    mount(columns, column);
}

function changeModalContent(type, modalObj, comps) {
    const { el, mount } = redom;
    modalObj.modalContent.innerHTML = '';
    for (const elem of comps) {
        const columns = el('div.columns.build');
        if (select && elem._id == select[type].id) {
            prevSelect = columns;
            columns.classList.add('select');
        }
        const img = el('div.column.card-image', el('img.img-responsive', { src: elem.imgurl }));
        const name = el('div.column.h5');
        name.innerText = elem.title;
        const firm = el('div.column.h5');
        firm.innerText = elem.firm;
        const desc = el('div.column.text-gray');
        desc.innerText = elem.description;
        const price = el('div.column.h5');
        price.innerText = elem.price + ' руб.';
        mount(modalObj.modalContent, columns);
        mount(columns, img);
        mount(columns, name);
        mount(columns, desc);
        mount(columns, firm);
        mount(columns, price);
        columns.addEventListener('click', () => {
            if (prevSelect)
                prevSelect.classList.remove('select');
            columns.classList.add('select');
            prevSelect = columns;
            select[type] = { imgurl: elem.imgurl, name, desc, price, id: elem._id, firm };
            selectBuild[type] = elem._id;
        });
    }
}

function setChecked(inpsType) {
    for (const inp of inpsType) {
        inp.addEventListener('change', () => {
            for (const inp2 of inpsType) {
                inp2.removeAttribute("checked");
            }
            inp.setAttribute("checked", "");
        });
    };
}