const tabs = [];
const aTabs = [];

const tabinit = async(mainEl) => {

    mainEl.innerHTML = '';

    const { el, mount, text } = redom;

    // Build
    const buildTab = el('li.tab-item');
    mount(mainEl, buildTab);
    let a = el('a.active', { style: { cursor: 'pointer' } });
    a.innerText = 'Подбор ПК';
    mount(buildTab, a);
    tabs.push(buildTab);
    aTabs.push(a);

    // Construct
    const constrTab = el('li.tab-item');
    mount(mainEl, constrTab);
    a = el('a', { style: { cursor: 'pointer' } })
    a.innerText = 'Конструктор';
    mount(constrTab, a);
    tabs.push(constrTab);
    aTabs.push(a);

    // ReadyBuild
    const readyBuildTab = el('li.tab-item');
    mount(mainEl, readyBuildTab);
    a = el('a', { style: { cursor: 'pointer' } });
    a.innerText = 'Готовые сборки';
    mount(readyBuildTab, a);
    tabs.push(readyBuildTab);
    aTabs.push(a);

    const session = getCookie('pcbuilduser');
    let ses;
    if (session && session.length > 0) ses = await checksession(session);


    if (session && session.length > 0) {
        if (ses && ses._id && ses._id.length > 0 && ses.type == 'admin' || ses.type == 'root') {
            // ReadyBuild
            const setPerTab = el('li.tab-item');
            mount(mainEl, setPerTab);
            a = el('a', { style: { cursor: 'pointer' } });
            a.innerText = 'Настройка прав доступа';
            mount(setPerTab, a);
            tabs.push(setPerTab);
            aTabs.push(a)
        }

        // Exit	
        //Create Exit Button
        const btnExit = el('button.btn.btn-error.mx-2');
        btnExit.innerText = 'Выйти';
        mount(mainEl, btnExit);

        btnExit.addEventListener('click', () => exit());

        return true;
    }


    // SiggnIn
    const signTab = el('li.tab-item');
    mount(mainEl, signTab);
    a = el('a', { style: { cursor: 'pointer' } });
    a.innerText = 'Вход/Регистрация';
    mount(signTab, a);
    tabs.push(signTab);
    aTabs.push(a);
    return false;
}