const puppeteer = require('puppeteer');
const request = require("request");
const fs = require("fs");


(async() => {

    const allPagesCount = 12;
    const pageCounts = 6;
    const fin = [];

    for (let index = 0; index < allPagesCount; index += pageCounts) {
        const URLS = {};
        const all = [];
        const pageStart = index;
        let p = pageStart;

        for (p; p < pageStart + pageCounts; p++) {
            const template = `https://www.dns-shop.ru/catalog/8a9ddfba20724e77/ssd-nakopiteli/?p=${p + 1}&order=1&groupBy=none&stock=2`
            URLS[p] = template;
        }


        for (let ii in URLS) {
            res(URLS[ii], parseFloat(ii)).then(result => {
                console.log('--------------------------FINISHING-----------------------------')
                all.push(result);
                console.log('END');
            });
        }

        let int;
        await new Promise(rslv => int = setInterval(() => {
            if (all.length === pageCounts) {
                rslv();
                clearInterval(int);
                console.log('PACK FINISHING');

                for (const obj of all) {
                    fin.push(obj)
                }

            }

        }), 1e3);
        console.log(fin.length);
        console.log(allPagesCount);
        await new Promise(rslv => {
            setTimeout(() => {
                rslv();
            }, 5000);
        })
    }

    let int;

    await new Promise(rslv => int = setInterval(() => {
        if (fin.length === allPagesCount) {
            rslv();
            clearInterval(int);
            console.log('SUPER FINISH');

            const rrr = [];

            for (const obj of fin) {
                rrr.push(...obj)
            }

            //Write result to file
            fs.writeFile(`ssd.json`, JSON.stringify(rrr), function(err) {
                if (err) {
                    console.log(err);
                }
            });
        }

    }), 1e3);

    // for (let pages = 0; pages < urls.length; pages+= 10) {
    // 	const urlsPack = urls.slice(pages, pages + 10);
    // }

})();

function res(urlll, page) {
    console.log('START');
    return new Promise(resolve => {
        // let rslv;
        // prms.push();

        (async() => {
            const browser = await puppeteer.launch();
            const page = await browser.newPage();
            var finish = [];

            const urll = urlll;
            const finishPage = [];
            await page.goto(urll);
            await page.waitFor(1000);

            const products = await page.evaluate(() => {
                const _products = {};
                const items = document.querySelectorAll('[data-id="product"].catalog-item-inner.catalog-product.has-avails');

                for (let i = 0; i < items.length; i++) {
                    const item = items[i];

                    const a = item.querySelector('.product-info .title a');
                    if (!a) {
                        continue;
                    }

                    const title = a.innerText.trim();
                    const descDiv = item.querySelector('.product-info .title span');
                    var desc = '';

                    if (descDiv) {
                        desc = descDiv.innerText.trim();
                    }

                    const priceSpan = item.querySelector('.product-price .price_g span');
                    if (!priceSpan) {
                        continue;
                    }

                    const price = parseFloat(priceSpan.innerText.trim().replace(/\s/g, ''));

                    const url = 'https://www.dns-shop.ru' + a.getAttribute('href').trim();

                    _products[i] = {
                        url: url,
                        title: title,
                        description: desc,
                        price: price
                    };

                }

                return _products
            });

            console.log(`Finish adverts page ${urll}`);

            //  This is main download function which takes the url of your image
            var download = function(uri, filename, callback) {
                request.head(uri, function(err, res, body) {
                    request(uri)
                        .pipe(fs.createWriteStream(filename))
                        .on("close", callback);
                });
            }

            var adverts = [];
            for (const key in products) {
                if (products.hasOwnProperty(key)) {
                    const element = products[key];
                    const url = element['url'];
                    console.log(`Go to Advert number: ${key} now`);
                    await page.goto(url + '/characteristics/');
                    await page.waitFor(1000);
                    const advert = await page.evaluate(() => {

                        var result = {};

                        const modelInfoTable = document.querySelector('.table-params.table-no-bordered');

                        if (modelInfoTable) {
                            const trs = modelInfoTable.querySelectorAll('tr');

                            for (let ll = 0; ll < trs.length; ll++) {
                                const tr = trs[ll];
                                const tds = tr.querySelectorAll('td');
                                if (tds.length < 2) {
                                    continue;
                                }

                                const field = tds[0].querySelector('.dots span').innerText.trim();

                                result[field] = tds[1].innerText.trim();

                                if (field == "Модель") {
                                    const model = tds[1].innerText.trim();
                                    result["firm"] = model.substr(0, model.indexOf(" "));
                                }
                            }

                        }

                        const imageURL = document.querySelector('.img a img.loading');
                        if (imageURL) {
                            const imgurl = imageURL.getAttribute('src');
                            let dataTitle = imageURL.getAttribute('data-title');
                            result['imgurl'] = imgurl;
                            dataTitle = dataTitle.replace(/\//g, "-");
                            dataTitle = dataTitle.replace(/ /g, '_');
                            dataTitle += ".jpg";
                            result['imgtitle'] = dataTitle;
                        }



                        return result;

                    });

                    advert['title'] = element['title'];
                    advert['description'] = element['description'];
                    advert['price'] = element['price'];

                    adverts.push({
                        advert: advert
                    })

                    if (advert['imgurl']) {
                        download(advert['imgurl'], advert['imgtitle'], function() {
                            console.log("Image ", advert['imgtitle'], "downloaded");
                        });
                    }

                    // console.log('get ',key,' from ', products.length);
                    console.log(`Get ${key} from ${products.length}`);
                }
            }
            // console.log(JSON.stringify(adverts));
            finish.push(...adverts);

            finishPage.push(...adverts);
            const jsonFinishPage = JSON.stringify(finishPage);
            console.log('All advers by this page finish');
            console.log('------------------------------');

            await browser.close();

            // const jsonFinish = JSON.stringify(finish);
            // console.log(jsonFinish);

            // fs.writeFile(`OZU_page_${parseFloat(page) + 1}.json`, jsonFinish, function (err) {
            // 	if (err) {
            // 		console.log(err);
            // 	}
            // });

            resolve(finish);
            console.log('FINSIH');
            // rslv();
        })();
    });
}